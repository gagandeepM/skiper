//
//  SKManager.swift
//  Skiper
//
//  Created by Gagandeep Mishra on 02/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import Foundation
import UIKit

struct SkiperURL {
    
    static let baseURL = "http://skiper.us/skiperapi/mobile/"
    
    static let login = baseURL+"login"
    static let activeJobs = baseURL+"getAllActiveJobs"
    static let currentLocation = baseURL+"setCurrentLocation"
    static let getJobNotification = baseURL+"getJobNotification"
    static let acceptRejectJob = baseURL+"acceptRejectJob"
    static let addPaymentDetail = baseURL+"addPaymentDetail"
    static let getChatList = baseURL+"getChatList"
    static let getSingleChat = baseURL+"getSingleChat"
    static let sendMessage = baseURL+"sendMessage"
    static let getSingleMessage = baseURL+"getSingleMessage"
}

struct UserDefaultsConstant {
    static let userDefaults = UserDefaults.standard
    static let token = "token"
    static let technicianId = "technicianId"
    static let deviceToken = "device_token"
}

struct MessageStrings {
    
    static let emptyEmail = "Email field cannot be empty."
    static let validEmail = "Enter valid email."
    static let emptyPassword = "Password field cannot be empty."
    static let locationPermission = "Skiper wants to access your location please enable location from settings."
    static let corruptData = "Data is corrupted!"
    
    static let emptyCash = "Please fill cash."
    static let emptyCheck = "Please fill check."
    static let emptyCard = "Please fill credit."
    static let paymentOption = "Please fill payment option."
    static let techParts = "Please fill technician parts."
    static let partsDescription = "Please fill parts description."
    static let totalAmount = "Please fill total amount."
    static let timeSpent = "Please fill time spent."
    
    static let apsAlertMessage = "Max accept job time has been exceeded and online system remove this job from your account"
}

