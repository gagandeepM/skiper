//
//  AppDelegate.swift
//  Skiper
//
//  Created by Gagandeep Mishra on 02/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import UserNotifications
import IQKeyboardManagerSwift
import SVProgressHUD

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    static let appInstance: AppDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }()
    
    var window: UIWindow?
    fileprivate var locationManager: CLLocationManager?
    fileprivate var updatedLocation: CLLocation?
    private var bannerTop : NSLayoutConstraint?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        settingConfiguration()
        
        setUpInitialViewController()
        
        setupLocationManager()
        
        configurePushNotification()
        
        //AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(launchOptions)")
        checkAPNS(launchOptions)
        
        return true
    }
    
    //MARK: - On Application Resign state Handling APNS
    
    private func checkAPNS(_ launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        if let remoteNotif = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? Dictionary<String, Any> {
            if let aps = remoteNotif["aps"] as? [String: Any],
                let isManual = aps["ismanuall"] as? Bool {
                if let viewController = self.window?.rootViewController,
                    let navViewController = viewController as? UINavigationController,
                    let tabController = navViewController.visibleViewController as? TabBarController {
                    if isManual {
                        tabController.selectedIndex = 0
                    } else {
                        tabController.selectedIndex = 1
                    }
                }
            }
        }
    }
    
    //MARK: - Setting Global View
    
    func settingConfiguration() {
        
        // MARK: - NavigationBar Appearace
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.barTintColor = UIColor.init(red: 61/255.0, green: 69/255.0, blue: 127/255.0, alpha: 1.0)
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        // MARK: - IQKeyboardManager
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [ChatViewController.self, CollectedPaymentController.self]
        IQKeyboardManager.sharedManager().disabledDistanceHandlingClasses = [ChatViewController.self]
        
        // MARK: - SVProgressHud
        
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setBackgroundColor(UIColor.init(red: 61/255.0, green: 69/255.0, blue: 127/255.0, alpha: 1.0))
        SVProgressHUD.setRingThickness(4.0)
    }
    
    //MARK: - Setting Initial View Controller
    
    func setUpInitialViewController() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
        let tabBarController = storyBoard.instantiateViewController(withIdentifier: "TabBarController")
        
        if let navigationController = self.window?.rootViewController as? UINavigationController {
            if UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) != nil {
                navigationController.viewControllers = [tabBarController]
            } else {
                navigationController.viewControllers = [loginController]
            }
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }
        
    }
    
    //MARK: - Location Based Functionality
    
    func setupLocationManager(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        self.locationManager?.requestAlwaysAuthorization()
        locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    //MARK: - Configure Push Notification
    
    private func configurePushNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            //UNUserNotificationCenter.current().delegate = self
            self.getNotificationSettings()
        }
    }
    
    private func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = 0
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //MARK: - APNS ****Push Notification****
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UserDefaultsConstant.userDefaults.set(token, forKey: UserDefaultsConstant.deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        if let aps = userInfo["aps"] as? [String: Any], let title = aps["alert"] as? String, let subtitle = aps["data"] as? String {
            if aps["ismanuall"] != nil {
                if let isManual = aps["ismanuall"] as? Bool {
                    showingJobNotificationAlert(jobTitle: title, jobSubtitle: subtitle, isManual: isManual)
                }
            } else {
                
                if MessageStrings.apsAlertMessage == aps["alert"] as? String ?? "" {
                    notificationSettingForTimeLimit(title: title, subTitle: subtitle)
                } else {
                    notificationSettingForChat(aps, title: title, subTitle: subtitle)
                }
            }
        }
        
    }
    private func notificationSettingForTimeLimit(title: String, subTitle: String) {
        
        AlertMessage.displayAlertMessage(titleMessage: title, alertMsg: subTitle)
        
        if let controller = UIApplication.topViewController() as? NotificationViewController {
            controller.viewWillAppear(true)
        }
        
        /*if let window = self.window {
         if var viewController = window.rootViewController {
         if(viewController is UINavigationController){
         viewController = (viewController as! UINavigationController).visibleViewController!
         }
         if let tabController = viewController as? UITabBarController, let selected = tabController.selectedViewController, let navController = selected as? UINavigationController, let controller = navController.visibleViewController as? NotificationViewController {
         controller.viewWillAppear(true)
         }
         }
         }*/
    }
    
    private func notificationSettingForChat(_ aps: [String: Any], title: String, subTitle: String) {
        
        let apsUserInfo = ["jobId": aps["job_id"] as? String ?? "", "messageId": aps["message_id"] as? Int ?? 0] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name("loadMessage"), object: nil, userInfo: apsUserInfo)
        
        if let childController = UIApplication.topViewController() {
            if !(childController is ChatViewController) {
                if let parentController = UIApplication.parentViewController(controller: childController) as? UINavigationController, let tabController = UIApplication.parentViewController(controller: parentController) as? UITabBarController {
                    if tabController.selectedIndex != 2 {
                        tabController.tabBar.items?[2].badgeColor = UIColor.red
                        tabController.tabBar.items?[2].badgeValue = "New"
                    }
                    AlertMessage.displayAlertMessage(titleMessage: title, alertMsg: subTitle)
                }
            }
        }
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    //MARK: - APNS ****Show Alert****
    
    private func showingJobNotificationAlert(jobTitle: String, jobSubtitle: String, isManual: Bool) {
        
        let alertController = UIAlertController(title: jobTitle, message: jobSubtitle, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            if let window = self.window {
                if var viewController = window.rootViewController {
                    // handle navigation controllers
                    if(viewController is UINavigationController){
                        viewController = (viewController as! UINavigationController).visibleViewController!
                    }
                    if viewController is TabBarController {
                        NotificationCenter.default.post(name: NSNotification.Name("NotifyJob"), object: nil, userInfo: ["isManual": isManual])
                    }
                }
            }
        }))
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - LOGOUT FUNCTION
    
    func logOut() {
        UserDefaultsConstant.userDefaults.removeObject(forKey: UserDefaultsConstant.token)
        UserDefaultsConstant.userDefaults.removeObject(forKey: UserDefaultsConstant.technicianId)
        DispatchQueue.main.async {
            self.setUpInitialViewController()
        }
    }
    
    //MARK: - App States
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //setupLocationManager()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        setupLocationManager()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Skiper")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Opening Settings
    
    func openSettingsForLocationService() {
        let alertController = UIAlertController(title: nil, message: MessageStrings.locationPermission, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        let secondAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(settingsAction)
        alertController.addAction(secondAction)
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Sending Location to Server
    
    func commonURLCallingFunction(userid: String, latitude: String, longitude: String) {
        DispatchQueue.global(qos: .background).async {
            self.sendCurrentLocationToServer(userId: userid, latitude: latitude, longitude: longitude)
        }
    }
    
    func sendCurrentLocationToServer(userId: String, latitude: String, longitude: String) {
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let urlString = SkiperURL.currentLocation
        let parameters = ["userId": userId, "latitude": latitude, "longitude": longitude, "token": token]
        HTTPClientAPI.instance().makeAPICall(url: urlString, params: parameters, method: .POST, success: { (data, response, error) in
            if error == nil {
                guard let data = data else {return}
                let serverResponse = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let locationResponse = serverResponse as? [String: Any] {
                    print(locationResponse)
                }
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }) { (data, response, error) in
            AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
        }
    }
    
}
// MARK: - Location Manager Delegates
@available(iOS 10.0, *)
extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                openSettingsForLocationService()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager?.startUpdatingLocation()
                locationManager?.startMonitoringSignificantLocationChanges()
            default:
                break
            }
        } else {
            print("Location services are not enabled")
            openSettingsForLocationService()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
            if !technicianId.isEmpty {
                if updatedLocation == nil {
                    updatedLocation = location
                    let latitude = "\(location.coordinate.latitude)"
                    let longitude = "\(location.coordinate.longitude)"
                    commonURLCallingFunction(userid: technicianId, latitude: latitude, longitude: longitude)
                } else {
                    let distance = updatedLocation?.distance(from: location)
                    if Double(distance!) >= 100 {
                        updatedLocation = location
                        print("\(Double(distance!))")
                        let latitude = "\(location.coordinate.latitude)"
                        let longitude = "\(location.coordinate.longitude)"
                        commonURLCallingFunction(userid: technicianId, latitude: latitude, longitude: longitude)
                    }
                }
            }
        }
    }
}
