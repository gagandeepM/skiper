//
//  PaymentOptionController.swift
//  Skiper
//
//  Created by Mayank juyal on 27/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

struct PaymentOption {
    var id: String
    var option: String
}

protocol PaymentOptionDelegate: class {
    func paymentOption(_ option: PaymentOption)
}

class PaymentOptionController: UIViewController {
    
    weak var delegate: PaymentOptionDelegate?
    
    //fileprivate let paymentOptions = [["id": "cash", "option": "Cash"], ["id": "credit", "option": "Credit Card"], ["id": "check", "option": "Check"], ["id": "cash-check", "option": "Split Cash/Check"], ["id": "cash-credit", "option": "Split Cash/Credit Card"], ["id": "check-credit", "option": "Split Check/Credit Card"], ["id": "cash-credit-check", "option": "Split Cash/Credit Card/Check"]]
    
    
    fileprivate var paymentOptions: [PaymentOption] = {
        var paymentOption: [PaymentOption] = [PaymentOption]()
        paymentOption.append(PaymentOption(id: "cash", option: "Cash"))
        paymentOption.append(PaymentOption(id: "credit", option: "Credit"))
        paymentOption.append(PaymentOption(id: "check", option: "Check"))
        paymentOption.append(PaymentOption(id: "cash-check", option: "Split Cash/Check"))
        paymentOption.append(PaymentOption(id: "cash-credit", option: "Split Cash/Credit Card"))
        paymentOption.append(PaymentOption(id: "check-credit", option: "Split Check/Credit Card"))
        paymentOption.append(PaymentOption(id: "cash-credit-check", option: "Split Cash/Credit Card/Check"))
        
        return paymentOption
        
    }()
    
    @IBOutlet weak var optionTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.preferredContentSize = CGSize(width: self.view.frame.width-20, height: 300)
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension PaymentOptionController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") {
            cell.textLabel?.text = paymentOptions[indexPath.row].option
            return cell
        }
        return UITableViewCell()
    }
}

extension PaymentOptionController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOption = paymentOptions[indexPath.row]
        self.delegate?.paymentOption(selectedOption)
        self.dismiss(animated: true, completion: nil)
    }
}
