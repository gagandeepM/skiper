//
//  CollectedPaymentViewModel.swift
//  Skiper
//
//  Created by Mayank juyal on 01/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class CollectedPaymentViewModel {
    
    var prefixString = 0
    
    fileprivate var timeIntervalArray: [[String: String]] = [[String: String]]()
    
    var isLoading: Bool = false {
        didSet{
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet{
            self.showSuccessAlertClosure?()
        }
    }
    
    var textFieldsValidated: (Bool, String) = (false, "") {
        didSet{
            self.updateValidationStatus?(textFieldsValidated.0, textFieldsValidated.1)
        }
    }
    
    //MARK: - Closures ViewModel
    
    var updateLoadingStatus:(()->())?
    
    var showSuccessAlertClosure:(()->())?
    
    var updateValidationStatus:((_ booleanValue: Bool, _ message: String)->())?
    
    var numberOfComponents: Int {
        return timeIntervalArray.count
    }
    
    //MARK: - Check Empty TextFields
    
    func checkingEmptyField(textModel: CollectPaymentTextModel) {
        if textModel.timeSpent?.isStringEmpty ?? true {
            
            self.textFieldsValidated = (false, MessageStrings.timeSpent)
            
        } else if textModel.jobPaidWith?.isStringEmpty ?? true {
            
            self.textFieldsValidated = (false, MessageStrings.paymentOption)
            
        } else if !checkingExtraTextField(textModel).0 {
            
            self.textFieldsValidated = (false, checkingExtraTextField(textModel).1)
            
        } else if textModel.techParts?.isStringEmpty ?? true {
            
            self.textFieldsValidated = (false, MessageStrings.techParts)
            
        } else if textModel.partsDescription?.isStringEmpty ?? true {
            
            self.textFieldsValidated = (false, MessageStrings.partsDescription)
            
        } else if textModel.totalAmount?.isStringEmpty ?? true {
            
            self.textFieldsValidated = (false, MessageStrings.totalAmount)
            
        } else {
            self.textFieldsValidated = (true, "")
        }
    }
    
    func checkingExtraTextField(_ textModel: CollectPaymentTextModel) -> (Bool, String) {
        if let jobPaidWith = textModel.jobPaidKey {
            switch jobPaidWith {
            case "cash":
                
                if textModel.cash?.isStringEmpty ?? true {
                    return (false, MessageStrings.emptyCash)
                } else {
                    return (true, "")
                }
                
            case "credit":
                
                if textModel.credit?.isStringEmpty ?? true {
                    return (false, MessageStrings.emptyCard)
                } else {
                    return (true, "")
                }
            case "check":
                
                if textModel.check?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCheck)
                } else {
                    return(true, "")
                }
            case "cash-check":
                
                if textModel.cash?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCash)
                } else if textModel.check?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCheck)
                } else {
                    return(true, "")
                }
                
            case "cash-credit":
                
                if textModel.cash?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCash)
                } else if textModel.credit?.isStringEmpty ?? true {
                    
                    return(false, MessageStrings.emptyCard)
                    
                }  else {
                    return(true, "")
                }
                
            case "check-credit":
                
                if textModel.check?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCheck)
                } else if textModel.credit?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCard)
                } else {
                    return(true, "")
                }
                
            case "cash-credit-check":
                
                if textModel.cash?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCash)
                } else if textModel.check?.isStringEmpty ?? true {
                    return(false, MessageStrings.emptyCheck)
                } else if textModel.credit?.isStringEmpty ?? true {
                   return(false, MessageStrings.emptyCard)
                } else {
                    return(true, "")
                }
                
            default:
                return(true, "")
            }
        }
        return(false, "")
    }
    
    //MARK: - Generating Time Interval Array
    
    func generatingTimeIntervals() {
        for i in 1...20 {
            let formattedValue = convertingToHourFormat(value: i*15)
            let resultValue = ["rawvalue": "\(i*15)", "showvalue": formattedValue]
            timeIntervalArray.append(resultValue)
        }
    }
    
    private func convertingToHourFormat(value: Int) -> String {
        if value >= 60 {
            if value%60==0 {
                prefixString = value/60
                return("\(prefixString):00")
            } else {
                let formattedString = "\(prefixString):\(value-(prefixString * 60))"
                return(formattedString)
            }
        } else {
            return("0\(prefixString):\(value)")
        }
    }
    
    //MARK: - Getting Specific Time Interval
    
    func getTimeIntervals(index: Int) -> [String: String] {
        let returningValue = timeIntervalArray[index]
        return returningValue
    }
    
    // MARK: - ADD PAYMENT DETAIL API
    
     func callAddPaymentDetailAPI(urlString: String, parameters: Dictionary<String, Any>) {
        
        self.isLoading = true
        
        HTTPClientAPI.instance().makeAPICall(url: urlString, params: parameters, method: .POST, success: { (data, response, error) in
            self.isLoading = false
            if error == nil {
                guard let data = data else {return}
                let response = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let response = response as? [String: Any], let status = response["status"] as? Int, let message = response["message"] as? String {
                    if status == 1 {
                        self.alertMessage = message
                    } else if status == 2 {
                        if #available(iOS 10.0, *) {
                            AppDelegate.appInstance.logOut()
                        } else {
                            // Fallback on earlier versions
                        }
                    } else {
                        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
                    }
                }
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }) { (data, response, error) in
            self.isLoading = false
            if error == nil {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(response?.statusCode ?? 0)")
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }
    }
    
    // MARK: - Creating Heading Arrays
    
    func creatingHeadingArrays(_ paymentOption: String = "cash") -> [String] {
        switch paymentOption {
        case "cash":
            let array = ["Time Spent:", "JOB Paid With:", "CASH:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "credit":
            let array = ["Time Spent:", "JOB Paid With:", "CREDIT:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "check":
            let array = ["Time Spent:", "JOB Paid With:", "CHECK:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "cash-check":
            let array = ["Time Spent:", "JOB Paid With:", "CASH:", "CHECK:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "cash-credit":
            let array = ["Time Spent:", "JOB Paid With:", "CASH:", "CREDIT:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "check-credit":
            let array = ["Time Spent:", "JOB Paid With:", "CHECK:", "CREDIT:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        case "cash-credit-check":
            let array = ["Time Spent:", "JOB Paid With:", "CASH:", "CHECK:", "CREDIT:", "BALANCE DUE:", "TECHNICIAN PARTS:", "PARTS DESCRIPTION:", "TOTAL AMOUNT:"]
            return array
        default:
            return [String]()
        }
    }
    
    // MARK: - Assigning Values to Text Model
    
    func assigningValuesTo(_ model: CollectPaymentTextModel, textField: UITextField, headingText: String) {
        
        switch headingText {
        case "CASH:":
            model.cash = textField.text
            break
        case "CREDIT:":
            model.credit = textField.text
            break
        case "CHECK:":
            model.check = textField.text
            break
        case "BALANCE DUE:":
            model.balanceDue = textField.text
            break
        case "TECHNICIAN PARTS:":
            model.techParts = textField.text
            break
        case "PARTS DESCRIPTION:":
            model.partsDescription = textField.text
            break
        case "TOTAL AMOUNT:":
            model.totalAmount = textField.text
            break
        default:
            break
        }
    }
    
    // MARK: - Configuring Text To Cell TextField
    
    func configureTextTo(_ textField: UITextField, model: CollectPaymentTextModel, headingText: String) {
        switch headingText {
        case "Time Spent:":
            textField.text = model.timeSpent ?? ""
            break
        case "JOB Paid With:":
            textField.text = model.jobPaidWith ?? ""
            break
        case "CASH:":
            textField.text = model.cash ?? ""
            break
        case "CREDIT:":
            textField.text = model.credit ?? ""
            break
        case "CHECK:":
            textField.text = model.check ?? ""
            break
        case "BALANCE DUE:":
            textField.text = model.balanceDue ?? ""
            break
        case "TECHNICIAN PARTS:":
            textField.text = model.techParts ?? ""
            break
        case "PARTS DESCRIPTION:":
            textField.text = model.partsDescription ?? ""
            break
        case "TOTAL AMOUNT:":
            textField.text = model.totalAmount ?? ""
            break
        default:
            break
        }
    }
    
    
}
