//
//  CollectedPaymentController.swift
//  Skiper
//
//  Created by Mayank juyal on 22/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SVProgressHUD


class CollectedPaymentController: UIViewController {
    
    var jobId: String?
    
    lazy var textModel: CollectPaymentTextModel = {
        return CollectPaymentTextModel()
    }()
    
    lazy var collectedPaymentViewModel: CollectedPaymentViewModel = {
        return CollectedPaymentViewModel()
    }()
    
    @IBOutlet weak var pickerContainerView: UIView!
    @IBOutlet weak var timePickerView: UIPickerView!
    
    fileprivate var jobPaidKey: String?
    fileprivate var timeSpentKey: String?
    
    @IBOutlet weak var paymentTableView: UITableView!
    
    fileprivate var headingArray: [String] = [String()] {
        didSet{
            DispatchQueue.main.async {
                self.paymentTableView.reloadData()
            }
        }
    }
    
    fileprivate var optionTextField: UITextField!
    fileprivate var timeSpentTextField: UITextField!
    
    //MARK: - View Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Payment"
        self.tabBarController?.tabBar.isHidden = true
        pickerContainerView.isHidden = true
        self.headingArray = self.collectedPaymentViewModel.creatingHeadingArrays("cash")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpTapGesture()
        //BINDING View Model
        collectedPaymentViewModel.updateLoadingStatus = {[weak self] in
            let loading = self?.collectedPaymentViewModel.isLoading ?? false
            if loading {
                SVProgressHUD.show()
            } else {
                self?.hideLoader()
            }
        }
        
        collectedPaymentViewModel.updateValidationStatus = {[weak self] (value, message) in
            if !value {
                self?.showAlert(message)
            } else {
                self?.initializeCallingAddPaymentDetailURL()
            }
        }
        
        collectedPaymentViewModel.showSuccessAlertClosure = {[weak self] in
            let message = self?.collectedPaymentViewModel.alertMessage ?? ""
            self?.showSuccessAlert(message)
        }
        
        collectedPaymentViewModel.generatingTimeIntervals()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar .isHidden = false
    }
    
    // MARK: - ****Tap Gesture SetUp****
    
    fileprivate func setUpTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - **** Dismiss Keyboard ****
    
    @objc func dismissKeyboard(_ gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - **** Submit Total for Review ****
    
    @IBAction func submitTotalAction(_ sender: UIButton) {
        self.view.endEditing(true)
        collectedPaymentViewModel.checkingEmptyField(textModel: textModel)
    }
    
    private func initializeCallingAddPaymentDetailURL() {
        let jobId = self.jobId ?? ""
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let urlString = SkiperURL.addPaymentDetail
        let parameter = ["job_id":jobId,"userId":technicianId,"token":token,"paid_with":jobPaidKey ?? "","total_amount":textModel.totalAmount ?? "","time_spent":timeSpentKey ?? "","Technician_Parts":textModel.techParts ?? "","parts_description":textModel.partsDescription ?? "", "cash": textModel.cash ?? "", "credit": textModel.credit ?? "", "check":  textModel.check ?? "", "balancedue":  textModel.balanceDue ?? ""]
        collectedPaymentViewModel.callAddPaymentDetailAPI(urlString: urlString, parameters: parameter)
    }
    
    // MARK: - **** ALERTS ****
    
    fileprivate func showAlert(_ message: String) {
        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
    }
    fileprivate func showSuccessAlert(_ message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            self.popBack()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Configuring Cell
    
    fileprivate func configureCell(cell: CollectPaymentCell, obj: String, indexPath: IndexPath) {
        cell.headingLabel.text = obj
        cell.inputField.tag = 100+indexPath.row
        self.settingKeyboardToTextField(cell, indexPath: indexPath)
        self.assigningTextToTextFields(cell, indexPath: indexPath)
        cell.inputField.delegate = self
    }
    
    //MARK: - Assigning Values to TextField 
    
    private func assigningTextToTextFields(_ cell: CollectPaymentCell, indexPath: IndexPath) {
        if let headingText = cell.headingLabel.text {
            collectedPaymentViewModel.configureTextTo(cell.inputField, model: textModel, headingText: headingText)
        }
    }
    
    // MARK: - Assigning Keyboards to Textfield
    
    private func settingKeyboardToTextField(_ cell: CollectPaymentCell, indexPath: IndexPath) {
        
        if cell.headingLabel.text == "PARTS DESCRIPTION:" {
            cell.inputField.keyboardType = .default
        } else {
            cell.inputField.keyboardType = .decimalPad
        }
    }
    
    // MARK: - Present Payment Popover
    
    func presentingPopUp(_ textField: UITextField) {
        if let popController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionController") as? PaymentOptionController {
            
            popController.modalPresentationStyle = .popover
            popController.delegate = self
            let popover = popController.popoverPresentationController
            
            popover?.permittedArrowDirections = .up
            
            popover?.delegate = self
            
            popover?.sourceView = textField
            
            popover?.sourceRect = textField.bounds
            
            self.present(popController, animated: false, completion: nil)
            
        }
    }
    
    private func hideLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    private func popBack() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is MainViewTVC {
                DispatchQueue.main.async {
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
    
    //MARK: - Picker Bar Button Action
    
    @IBAction func cancelBarAction(_ sender: Any) {
        pickerContainerView.isHidden = true
        textModel.timeSpent = ""
        timeSpentTextField.text = ""
    }
    
    @IBAction func doneBarAction(_ sender: Any) {
        pickerContainerView.isHidden = true
        timeSpentTextField.text = textModel.timeSpent
    }
    
}

// MARK: - Tableview Datasource

extension CollectedPaymentController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "\(CollectPaymentCell.self)") as? CollectPaymentCell {
            let objString = headingArray[indexPath.row]
            self.configureCell(cell: cell, obj: objString, indexPath: indexPath)
            
            return cell
        }
        return UITableViewCell()
    }
}


// MARK: - UIPopoverPresentationControllerDelegate

extension CollectedPaymentController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: - TextField Delegate

extension CollectedPaymentController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 100 {
            self.view.endEditing(true)
            pickerContainerView.isHidden = false
            timeSpentTextField = textField
            textModel.timeSpent = collectedPaymentViewModel.getTimeIntervals(index: 0)["showvalue"]
            timeSpentTextField.text = textModel.timeSpent ?? ""
            return false
        }
        else if textField.tag == 101 {
            self.view.endEditing(true)
            presentingPopUp(textField)
            optionTextField = textField
            return false
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let tag = textField.tag-100
        let indexPath = IndexPath(row: tag, section: 0)
        
        if let cell = paymentTableView.cellForRow(at: indexPath) as? CollectPaymentCell, let headingText = cell.headingLabel.text {
            self.collectedPaymentViewModel.assigningValuesTo(textModel, textField: textField, headingText: headingText)
        }
    }
    
}

// MARK: - Payment Option Delegate

extension CollectedPaymentController: PaymentOptionDelegate {
    func paymentOption(_ option: PaymentOption){
        jobPaidKey = option.id
        textModel.jobPaidKey = option.id
        textModel.jobPaidWith = option.option
        optionTextField.text = textModel.jobPaidWith ?? ""
        textModel.cash = ""
        textModel.check = ""
        textModel.credit = ""
        self.headingArray = self.collectedPaymentViewModel.creatingHeadingArrays(option.id)
    }
}

// MARK: - UIPickerViewDelegate

extension CollectedPaymentController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return collectedPaymentViewModel.numberOfComponents
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return collectedPaymentViewModel.getTimeIntervals(index: row)["showvalue"]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        timeSpentKey = collectedPaymentViewModel.getTimeIntervals(index: row)["rawvalue"]
        textModel.timeSpent = collectedPaymentViewModel.getTimeIntervals(index: row)["showvalue"]
        timeSpentTextField.text = collectedPaymentViewModel.getTimeIntervals(index: row)["showvalue"]
    }
}

// MARK: - Text Model -

class CollectPaymentTextModel {
    var timeSpent: String?
    var jobPaidWith: String?
    var techParts: String?
    var partsDescription: String?
    var totalAmount: String?
    var cash: String?
    var credit: String?
    var check: String?
    var balanceDue: String?
    var jobPaidKey: String?
}
