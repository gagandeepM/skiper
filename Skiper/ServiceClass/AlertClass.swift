//
//  AlertClass.swift
//  Skiper
//
//  Created by Mayank juyal on 20/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class AlertMessage {
    
    private static var alertMessageController: UIAlertController!
    
    internal static func displayAlertMessage(titleMessage:String, alertMsg:String) {
        AlertMessage.alertMessageController = UIAlertController(title: titleMessage, message:
            alertMsg, preferredStyle: UIAlertControllerStyle.alert)
        AlertMessage.alertMessageController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
        if let controller = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            controller.present(AlertMessage.alertMessageController, animated: true, completion: nil)
        }
        else{
            DispatchQueue.main.async {
                UIApplication.shared.delegate?.window??.rootViewController?.present(AlertMessage.alertMessageController, animated: true, completion: nil)
            }
        }
        return
    }
    
}

