//
//  ServiceHandler.swift
//  Social Login
//
//  Created by abhishek on 05/12/17.
//  Copyright © 2017 abhishek. All rights reserved.
//

import Foundation
import UIKit

//MARK: - API CALL FUNCTION -


// MARK: - HTTP Methods

enum HTTPMethod :String {
    case  GET
    case  POST
    case  DELETE
    case  PUT
}

class HTTPClientAPI {
    
    typealias successClosure = ( Data? ,HTTPURLResponse? ,NSError? ) -> Void
    typealias failureClosure = ( Data? ,HTTPURLResponse? ,NSError? ) -> Void
    
    private var request : URLRequest?
    private var session : URLSession?
    
    static func instance() ->  HTTPClientAPI {
        return HTTPClientAPI()
    }
    
    func makeAPICall(url: String,params: Dictionary<String, Any>?, method: HTTPMethod, success: @escaping successClosure, failure: @escaping failureClosure) {
        guard let url = URL(string: url) else {
            //AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "url is wrong.")
            return
        }
        request = URLRequest(url: url)
        if let params = params {
            
            let  jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            request?.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request?.httpBody = jsonData//?.base64EncodedData()
            
        }
        request?.httpMethod = method.rawValue
        
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        session = URLSession(configuration: configuration)
        
        session?.dataTask(with: request!) { (data, response, error) -> Void in
            if let data = data {
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    success(data , response , error as NSError?)
                } else {
                    failure(data , response as? HTTPURLResponse, error as NSError?)
                }
            } else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
            }.resume()
    }
    
    //MARK: - Upload File To Server
    
    func uploadFileToServer(url: String, parameters: Dictionary<String, Any>, data: [Data], method: HTTPMethod, format: MessageType, success: @escaping successClosure, failure: @escaping failureClosure) {
        var mimeType = ""
        var fileName = ""
        switch format {
        case .photo:
            mimeType = "image/jpg"
            fileName = "image.jpg"
        case .file:
            mimeType = "file/txt"
            fileName = "file.txt"
        case .text:
            mimeType = ""
            fileName = ""
        }
        
        guard let url = URL(string: url) else { return }
        
        request = URLRequest(url: url)
        request?.httpMethod = method.rawValue
        let boundary = "Boundary-\(UUID().uuidString)"
        request?.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request?.httpBody = createBody(parameters: parameters, boundary: boundary, data: data, mimeType: mimeType, filename: fileName)
        
        
        request?.httpMethod = method.rawValue
        
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        session = URLSession(configuration: configuration)
        
        session?.dataTask(with: request!) { (data, response, error) -> Void in
            if let data = data {
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    success(data , response , error as NSError?)
                } else {
                    failure(data , response as? HTTPURLResponse, error as NSError?)
                }
            } else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
            }.resume()
        
    }
    
    private func createBody(parameters: Dictionary<String, Any>,
                            boundary: String,
                            data: [Data]?,
                            mimeType: String,
                            filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        
        if data?.count != 0 {
            var i = 1
            for dataItem in data! {
                let fileNammeee = "image\(i).jpg"
                print("Content-Disposition: form-data; name=\"image\"; filename=\"\(fileNammeee)\"\r\n")
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"image\(i)\"; filename=\"\(fileNammeee)\"\r\n")
                body.appendString("Content-Type: \(mimeType)\r\n\r\n")
                body.append(dataItem)
                body.appendString("\r\n")
                i += 1
                
            }
            body.appendString("--".appending(boundary.appending("--")))
        } else {
            body.appendString("\r\n")
            body.appendString("--".appending(boundary.appending("--")))
        }
        
        return body as Data
    }
}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
