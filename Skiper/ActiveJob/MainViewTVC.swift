//
//  MainViewTVC.swift
//  Skiper
//
//  Created by Gagandeep Mishra on 05/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SVProgressHUD

class MainViewTVC: UITableViewController {
    
    fileprivate var cellViewModel: [ActiveJobModel] = [ActiveJobModel]() {
        didSet{
            self.reloadingTableView()
        }
    }
    
    fileprivate lazy var activeJobFetchModel: ActiveJobsFetch = {
        return ActiveJobsFetch()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.isHidden = false
        //UIColor(colorLiteralRed: 61/255, green: 69/255, blue: 127/255, alpha: 1.0)
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        commonInitializeAPIHit()
    }
    
    fileprivate func commonInitializeAPIHit() {
        if let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String {
            SVProgressHUD.show()
            let urlString = SkiperURL.activeJobs
            let parameters = ["token": token, "offset": "0"]
            getActiveJobs(urlString: urlString, parameters: parameters)
        }
    }
    
    private func reloadingTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if cellViewModel.count == 0 {
            AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "No active job.")
        }
        
    }
    
    func hideProgressHud() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func getActiveJobs(urlString: String, parameters: Dictionary<String, Any>) {
        HTTPClientAPI.instance().makeAPICall(url: urlString, params: parameters, method: .POST, success: { (data, response, error) in
            self.hideProgressHud()
            if error == nil {
                guard let data = data else {return}
                let response = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                self.activeJobFetchModel.fetchActiveJobs(response: response, completion: { (success, message, list) in
                    if success {
                        if let list = list {
                            self.cellViewModel = list
                        }
                    } else {
                        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
                    }
                })
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "Error")
            }
        }) { (data, response, error) in
            self.hideProgressHud()
            if error == nil {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(response?.statusCode ?? 0)" )
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellViewModel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "activeJobCell", for: indexPath) as? ActiveJobCell {
            let activeJob = cellViewModel[indexPath.row]
            cell.configureCell(activeJob)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationObj = self.cellViewModel[indexPath.row]
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "\(ActiveJobDetailController.self)") as? ActiveJobDetailController {
            controller.activeJobDetail = notificationObj
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
