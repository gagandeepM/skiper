//
//  TabBarController.swift
//  Skiper
//
//  Created by Gagandeep Mishra on 06/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        self.navigationItem.leftBarButtonItem=nil;
        self.navigationItem.hidesBackButton=true;
        
        UITabBar.appearance().tintColor = UIColor.init(red: 61/255.0, green: 69/255.0, blue: 127/255.0, alpha: 1.0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(switchingTabs(_:)), name: NSNotification.Name("NotifyJob"), object: nil)
    }

    func switchingTabs(_ notification: NSNotification) {
        if let notification = notification.userInfo as? [String: Bool], let check = notification["isManual"]  {
            if check {
                if selectedIndex == 0 {
                    if let mainNavTVC = self.childViewControllers[0] as? UINavigationController,
                        let mainViewTVC = mainNavTVC.visibleViewController as? MainViewTVC{
                        mainViewTVC.viewWillAppear(true)
                    }
                }
            } else {
                if selectedIndex == 1 {
                    if let mainNavTVC = self.childViewControllers[1] as? UINavigationController, let notificationViewVC = mainNavTVC.visibleViewController as? NotificationViewController {
                        notificationViewVC.viewWillAppear(true)
                    }
                }
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 2 {
            self.tabBar.items?[2].badgeValue = nil
        }
    }
}

