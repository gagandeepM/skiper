//
//  ActiveJobDetailController.swift
//  Skiper
//
//  Created by Mayank juyal on 24/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class ActiveJobDetailController: UIViewController {
    
    @IBOutlet weak var detailTableView: UITableView!
    
    var activeJobDetail: ActiveJobModel?
    
    fileprivate var cellHeadingLabel: UILabel!
    fileprivate var cellSubHeadnglabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableView.estimatedRowHeight = 50.0
        detailTableView.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.title = "Job Detail"
        DispatchQueue.main.async {
            self.detailTableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Configuring Cell
    
    func configureCell(indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            cellHeadingLabel.text = "Doing Business As (DBA):"
            cellSubHeadnglabel.text = activeJobDetail?.businuess
            break
        case 1:
            cellHeadingLabel.text = "Customer Name:"
            let customerName = "\(activeJobDetail?.customerFirstName ?? "") \(activeJobDetail?.customerLastName ?? "")"
            cellSubHeadnglabel.text = customerName
            break
        case 2:
            cellHeadingLabel.text = "Customer Number:"
            cellSubHeadnglabel.text = activeJobDetail?.customer_phone
            break
        case 3:
            cellHeadingLabel.text = "Customer Address:"
            cellSubHeadnglabel.text = activeJobDetail?.address
            break
        case 4:
            cellHeadingLabel.text = "Job Description:"
            cellSubHeadnglabel.text = activeJobDetail?.job_Details
            break
        case 5:
            cellHeadingLabel.text = "Service Fee:"
            cellSubHeadnglabel.text = "$\(activeJobDetail?.service_fees ?? "")"
            break
        case 6:
            cellHeadingLabel.text = "Appointment time:"
            cellSubHeadnglabel.text = activeJobDetail?.appoinent_time
            break
        default:
            break
        }
    }
    
    //MARK: - I Collected Payment Action
    
    @IBAction func collectedPaymentAction(_ sender: UIButton) {
        if let collectedPaymentController = self.storyboard?.instantiateViewController(withIdentifier: "\(CollectedPaymentController.self)") as? CollectedPaymentController {
            collectedPaymentController.jobId = activeJobDetail?.jobId
            self.navigationController?.pushViewController(collectedPaymentController, animated: true)
        }
    }
    
}

//MARK: - TableView DataSource

extension ActiveJobDetailController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") {
            if let headingLabel = cell.contentView.viewWithTag(101) as? UILabel, let subHeadingLabel = cell.contentView.viewWithTag(102) as? UILabel {
                cellHeadingLabel = headingLabel
                cellSubHeadnglabel = subHeadingLabel
                self.configureCell(indexPath: indexPath)
            }
            return cell
        }
        return UITableViewCell()
    }
}
