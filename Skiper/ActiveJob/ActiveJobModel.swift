//
//  ActiveJobModel.swift
//  Skiper
//
//  Created by Mayank juyal on 21/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import Foundation

struct ActiveJobModel {
    var jobId: String?
    var job_Details: String?
    var job_type_name: String?
    var appoinent_time: String?
    var sortDateTime: String?
    var businuess: String?
    var service_fees: String?
    
    var customerFirstName: String?
    var customerLastName: String?
    var address: String?
    var customer_city: String?
    var customer_state: String?
    var customer_phone: String?
}

class ActiveJobsFetch {
    
    typealias completionClosure = (_ success: Bool, _ message: String, _ list: [ActiveJobModel]?) -> Void
    
    func fetchActiveJobs(response: Any?, completion: completionClosure) {
        
        var activeJobArray = [ActiveJobModel]()
        if let fetchedResult = response as? [String: Any], let status = fetchedResult["status"] as? Int, let message = fetchedResult["message"] as? String {
            if status == 1 {
                if let data = fetchedResult["data"] as? [[String: Any]] {
                    for item in data {
                        
                        let jobId = item["job_id"] as? String ?? ""
                        let job_Details = item["Job_Details"] as? String ?? ""
                        let job_type_name = item["job_type_name"] as? String ?? ""
                        let appoinent_time = item["appoinent_time"] as? String ?? ""
                        let customerFirstName = item["cutomer_FirstName"] as? String ?? ""
                        let customerLastName = item["cutomer_LastName"] as? String ?? ""
                        let address = item["Address"] as? String ?? ""
                        let customer_city = item["cutomer_city"] as? String ?? ""
                        let customer_state = item["customer_state"] as? String ?? ""
                        let customer_phone = item["customer_phone"] as? String ?? ""
                        let businuess = item["businuess"] as? String ?? ""
                        let service_fees = item["service_fees"] as? String ?? ""
                        let sortDateTime = item["sortDateTime"] as? String ?? ""
                        
                        let obj = ActiveJobModel(jobId: jobId, job_Details: job_Details, job_type_name: job_type_name, appoinent_time: appoinent_time, sortDateTime: sortDateTime, businuess: businuess, service_fees: service_fees, customerFirstName: customerFirstName, customerLastName: customerLastName, address: address, customer_city: customer_city, customer_state: customer_state, customer_phone: customer_phone)
                        
                        activeJobArray.append(obj)
                    }
                    completion(true, message, activeJobArray)
                }
            } else if status == 2 {
                if #available(iOS 10.0, *) {
                    AppDelegate.appInstance.logOut()
                } else {
                    // Fallback on earlier versions
                }
            } else {
                completion(false, message, nil)
            }
        }
    }
    
}
