//
//  LoginModel.swift
//  Skiper
//
//  Created by Mayank juyal on 20/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class LoginURLParse {
    
    typealias CompletionHandler = (_ success:Bool, _ message: String) -> Void
    
    func getTechnicianData(data: Data?, completionHandler: CompletionHandler) {
        guard let data = data else {
            print("Data is corrupted!!!!")
            return
        }
        let response = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        if let loginResponse = response as? [String: Any], let status = loginResponse["status"] as? Bool, let message = loginResponse["message"] as? String {
            if status {
                if let technicianData = loginResponse["data"] as? [String: Any] {
                    let technicianID = technicianData["Technician_ID"] as? String ?? ""
                    let token = technicianData["token"] as? String ?? ""
                    UserDefaultsConstant.userDefaults.set(token, forKey: UserDefaultsConstant.token)
                    UserDefaultsConstant.userDefaults.set(technicianID, forKey: UserDefaultsConstant.technicianId)
                }
            } else {
                print("Response Failed!!!!")
            }
            completionHandler(status, message )
        }
    }
    
}
