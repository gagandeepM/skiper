//
//  ViewController.swift
//  Skiper
//
//  Created by Gagandeep Mishra on 02/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginScrollView: UIScrollView!
    
    fileprivate var activeField: UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        //settingUpNotificationCenter()
        setUpTapGesture()
    }
    
    override func viewDidLayoutSubviews() {
        self.emailTextfield.setBottomBorder()
        self.passwordTextfield.setBottomBorder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow , object: nil)
//        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide , object: nil)
    }
    
    
    fileprivate func initializeView() {
        self.navigationController?.navigationBar.isHidden = true
        self.loginButton.layer.cornerRadius = 20.0
        self.loginButton.clipsToBounds = false
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
    }
    /*
    fileprivate func settingUpNotificationCenter() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: Notification.Name.UIKeyboardWillHide,
            object: nil)
    }*/
    
    fileprivate func setUpTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard(_ gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - Check For Empty TextFields
    
    func textValidation(_ textField: UITextField) -> (Bool, String) {
        if textField == emailTextfield {
            if textField.isTextFieldEmpty {
                return (false, MessageStrings.emptyEmail)
            } else {
                if (textField.text?.isValidEmail ?? false) {
                    return (true, "")
                } else {
                    return (false, MessageStrings.validEmail)
                }
            }
        } else {
            if textField.isTextFieldEmpty {
                return (false, MessageStrings.emptyPassword)
            } else {
                return (true, "")
            }
        }
    }
    
    
    // MARK: - UIKeyboard Notification Methods
    
    /*@objc private func keyboardWillShow(notification: NSNotification) {
        self.loginScrollView.isScrollEnabled = true
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            self.loginScrollView.contentInset = contentInsets
            self.loginScrollView.scrollIndicatorInsets = contentInsets
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize.height
            if let activeFieldPresent = activeField {
                if (!aRect.contains(activeFieldPresent.frame.origin)) {
                    self.loginScrollView.scrollRectToVisible(activeField!.frame, animated: true)
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize.height, 0.0)
            self.loginScrollView.contentInset = contentInsets
            self.loginScrollView.scrollIndicatorInsets = contentInsets
            self.view.endEditing(true)
            self.loginScrollView.isScrollEnabled = false
        }
    }*/
    
    // MARK: - Login Button Action
    @IBAction func didLoginButtonPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        //device_type": "2" for iOS
        
        let emailFieldValidate = textValidation(emailTextfield)
        let passwordFieldValidate = textValidation(passwordTextfield)
        
        if !emailFieldValidate.0 {
            AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: emailFieldValidate.1)
        } else if !passwordFieldValidate.0 {
            AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: passwordFieldValidate.1)
        } else {
            SVProgressHUD.show()
            
            let deviceToken = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.deviceToken) as? String ?? ""
            
            let urlString = SkiperURL.login
            let parameters = ["email_id": emailTextfield.text!, "password": passwordTextfield.text!, "device_token": deviceToken, "device_type": "2"]
            callLoginURL(urlString: urlString, parameters: parameters)
        }
    }
    
    // MARK: - Login URL HIT
    
    fileprivate func callLoginURL(urlString: String, parameters: Dictionary<String, Any>) {
       
        HTTPClientAPI.instance().makeAPICall(url: urlString, params: parameters, method: .POST, success: { (data, response, error) in
            
            self.hideProgressHud()
            
            if error == nil {
                let parseModel = LoginURLParse()
                parseModel.getTechnicianData(data: data, completionHandler: { (success, message) -> Void in
                    
                    if success {
                        self.navigateToHomeScreen()
                    } else {
                        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
                    }
                })
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
            
        }) { (data, response, error) in
            
            self.hideProgressHud()
            
            if error == nil {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(response?.statusCode ?? 0)" )
            } else {
            
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }
    }
    
    fileprivate func hideProgressHud() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    fileprivate func navigateToHomeScreen() {
        DispatchQueue.main.async {
            if let tabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController {
                self.navigationController?.pushViewController(tabBarController, animated: true)
            }
        }
    }
}

// MARK: - UITextField Delegates

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
}

// MARK: - UITextField Extension

extension UITextField {
    func setBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
}
