//
//  NotificationViewCell.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit


protocol ButtonActionDelegate: class {
    func tappedOnButton(status value: String, id: Int)
}


class NotificationViewCell: UITableViewCell {
    
    private let acceptKey = "1"
    private let rejectKey = "2"
    
    weak var buttonCustomDelegate: ButtonActionDelegate?
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var townAddressLabel: UILabel!
    @IBOutlet weak var appointmentTimeLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureCell(indexPath: IndexPath, notificationData: NotificationData) {
        jobTypeLabel.text = notificationData.job_type_name
        townAddressLabel.text = "\(notificationData.city)"
        appointmentTimeLabel.text = notificationData.appoinent_time
        acceptButton.tag = indexPath.row
        rejectButton.tag = indexPath.row
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        self.buttonCustomDelegate?.tappedOnButton(status: acceptKey, id: sender.tag)
    }

    @IBAction func rejectAction(_ sender: UIButton) {
        self.buttonCustomDelegate?.tappedOnButton(status: rejectKey, id: sender.tag)
    }
}
