//
//  CollectPaymentCell.swift
//  Skiper
//
//  Created by Mayank juyal on 22/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class CollectPaymentCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

