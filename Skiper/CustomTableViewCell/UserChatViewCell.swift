//
//  UserChatViewCell.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class UserChatViewCell: UITableViewCell {

    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var recieverMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureCell(_ message: Message) {
        //135-206-250
        //guard let image = UIImage(named: "chat_bubble_received") else { return }
        recieverMessageLabel.text = message.text ?? ""
        bubbleImageView.tintColor = UIColor(red: 135.0/255.0, green: 206.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        bubbleImageView.image = bubbleImageView.image?
            .resizableImage(withCapInsets:
                UIEdgeInsetsMake(17, 21, 17, 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)

    }
}
