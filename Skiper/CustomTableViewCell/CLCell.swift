//
//  CLCell.swift
//  Skiper
//
//  Created by Mayank juyal on 09/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class CLCell: UITableViewCell {
    @IBOutlet weak var titleCLLabel: UILabel!
    @IBOutlet weak var subTitleCLLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
