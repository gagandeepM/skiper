//
//  OfficeChatViewCell.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class OfficeChatViewCell: UITableViewCell {
    
    @IBOutlet weak var bubbleImageView: UIImageView!
    
    @IBOutlet weak var officeMessageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func configureCell(_ message: Message) {
        officeMessageLabel.text = message.text
        bubbleImageView.tintColor = UIColor.lightGray
        bubbleImageView.image = bubbleImageView.image?
            .resizableImage(withCapInsets:
                UIEdgeInsetsMake(17, 21, 17, 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
    }
}
