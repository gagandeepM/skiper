//
//  ActiveJobCell.swift
//  Skiper
//
//  Created by Mayank juyal on 21/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class ActiveJobCell: UITableViewCell {
    
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var subTitlelabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(_ activeJob: ActiveJobModel) {
        
        jobTitleLabel.attributedText = NSMutableAttributedString().boldUnderLined(activeJob.sortDateTime ?? "")
        
        subTitlelabel.text = activeJob.address
    }
    
}

extension NSMutableAttributedString {
    @discardableResult func boldUnderLined(_ text: String) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold), NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue as AnyObject]
        let boldUnderLinedString = NSMutableAttributedString(string: text, attributes:attrs)
        return boldUnderLinedString
    }
}
