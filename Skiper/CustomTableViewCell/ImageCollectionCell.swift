//
//  ImageCollectionCell.swift
//  Skiper
//
//  Created by Mayank juyal on 12/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SDWebImage
class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var chatImageView: UIImageView!
    
    func configureCell(_ imageUrl: File) {
        let urlString = imageUrl.file ?? ""
        print(urlString)
        
        chatImageView.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "ImagePlaceholder"))
        
    }
}
