//
//  ChatImageCell.swift
//  Skiper
//
//  Created by Mayank juyal on 12/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class ChatImageCell: UITableViewCell {
    
    
    fileprivate let rootViewController = UIApplication.shared.delegate?.window??.rootViewController
    
    
    fileprivate var imageArray: [File] = [File]() {
        didSet{
            DispatchQueue.main.async {
                self.chatCollectionView.reloadData()
            }
        }
    }
    
    @IBOutlet weak var containerBackground: UIView!
    @IBOutlet weak var chatCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func configureCell(_ file: [File], owner: MessageOwner) {
        self.chatCollectionView.dataSource = self
        self.chatCollectionView.delegate = self
        if owner == .Admin {
            self.chatCollectionView.backgroundColor = UIColor.gray
        } else {
            self.chatCollectionView.backgroundColor = UIColor(red: 135.0/255.0, green: 206.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        }
        imageArray = file
    }
    
    var scrollContentView = UIView()
    fileprivate var scrollImageView: UIImageView = UIImageView()
    
    @objc fileprivate func tapAction(_ imageView: UIImageView, controller: UIViewController) {
        scrollContentView.frame = UIScreen.main.bounds
        scrollContentView.backgroundColor = UIColor.white
        let scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.frame  = scrollContentView.frame
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        let imageView = imageView
        scrollImageView = UIImageView(image: imageView.image)
        scrollImageView.frame = UIScreen.main.bounds
        scrollImageView.backgroundColor = .black
        scrollImageView.contentMode = .scaleAspectFit
        scrollImageView.isUserInteractionEnabled = true
        scrollView.addSubview(scrollImageView)
        scrollContentView.addSubview(scrollView)
        scrollContentView.addSubview(creatingCancelButton(scrollContentView))
        controller.view.addSubview(scrollContentView)
    }
    private func creatingCancelButton(_ container: UIView) -> UIButton {
        let crossButton = UIButton(frame: CGRect(x: 0, y: 10, width: 50, height: 50))
        crossButton.setTitle("<", for: .normal)
        crossButton.setTitleColor(UIColor.white, for: .normal)
        crossButton.addTarget(self, action: #selector(dismissImage(_:)), for: .touchUpInside)
        return crossButton
    }
    @objc private func dismissImage(_ sender: UIButton) {
        scrollContentView.removeFromSuperview()
    }
}

extension ChatImageCell: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollImageView
    }
}

extension ChatImageCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as? ImageCollectionCell {
            print(imageArray.count)
            let file = imageArray[indexPath.row]
            cell.configureCell(file)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionCell, let controller = rootViewController {
            controller.view.endEditing(true)
            self.tapAction(cell.chatImageView, controller: controller)
        }
        
    }
}
