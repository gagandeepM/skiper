//
//  ChatViewController.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MobileCoreServices
import SVProgressHUD
import OpalImagePicker

class ChatViewController: UIViewController {
    
    private var offsetMultiplier = 1
    
    var clModel: CLModel = CLModel()
    
    fileprivate lazy var chatViewModel: ChatViewModel = {
        return ChatViewModel()
    }()
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:
            #selector(ChatViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white
        refreshControl.layoutIfNeeded()
        return refreshControl
    }()
    
    private var maxHeight: CGFloat = 80.0
    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var textContainerBottomCons: NSLayoutConstraint!
    @IBOutlet weak var textViewChat: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var textContainerHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var textFieldConatinerView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        settingUpView()
        setUpnotificationCenter()			
        //setUpTapGesture()
        
        initViewModel()
    }
    
    
    private func setUpnotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(receivedNotification(_:)), name: NSNotification.Name("loadMessage"), object: nil)
    }
    
    func receivedNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo as? [String: Any], let jobId = userInfo["jobId"] as? String, let messageId = userInfo["messageId"] as? Int {
            if clModel.jobid == jobId {
                chatViewModel.recieveMessage("\(messageId)")
            }
        }
    }
    
    private func initViewModel() {
        chatViewModel.showAlertMessage = { [weak self] in
            let alertMessage = self?.chatViewModel.alertMessage ?? ""
            self?.showAlert(alertMessage)
        }
        chatViewModel.reloadTableViewClosure = { [weak self] in
            self?.reloadTableView()
        }
        chatViewModel.updateLoadingStatus = { [weak self] in
            let isLoading = self?.chatViewModel.isLoading ?? false
            if isLoading {
                SVProgressHUD.show()
            } else {
                self?.hideProgressHud()
            }
        }
        let jobId = clModel.jobid ?? ""
        chatViewModel.getPreviousChatFor(jobId, offset: "0")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
        settingUpNotificationCenter()
    }
    
    fileprivate func settingUpView() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        chatTableView.estimatedRowHeight = 80.0
        chatTableView.rowHeight = UITableViewAutomaticDimension
        self.tabBarController?.tabBar.isHidden = true
        self.chatTableView.addSubview(refreshControl)
        chatViewModel.scrollToBottom(self.chatTableView, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
//    fileprivate func setUpTapGesture() {
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        tapGesture.numberOfTapsRequired = 1
//        chatTableView.addGestureRecognizer(tapGesture)
//    }
    
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.chatTableView.reloadData()
            self.chatViewModel.scrollToBottom(self.chatTableView, animated: true)
        }
    }
    
    private func hideProgressHud() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    private func showAlert(_ message: String) {
        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
    }
    
    
    @objc func dismissKeyboard(_ gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    fileprivate func settingUpNotificationCenter() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: Notification.Name.UIKeyboardWillHide,
            object: nil)
        textViewChat.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let tv = object as? UITextView {
            handlingLayoutOfTextView(textView: tv)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow , object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide , object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("loadMessage"), object: nil)
        textViewChat.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
    
    private func handlingLayoutOfTextView(textView: UITextView) {
        if textView.text.contains(UIPasteboard.general.string ?? "") {
            if textView.contentSize.height < maxHeight {
                self.textContainerHeightConstraint?.constant = textView.contentSize.height+10
            } else {
                self.textContainerHeightConstraint?.constant = 80
            }
        } else {
            if textView.contentSize.height < maxHeight {
                self.textContainerHeightConstraint?.constant = textView.contentSize.height+10
            }
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    // MARK: - UIKeyboard Notification Methods
    
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            
            textContainerBottomCons.constant = chatViewModel.calculateKeyboardHeight(keyboardFrame)
            
            chatViewModel.scrollToBottom(self.chatTableView, animated: true)
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        textContainerBottomCons.constant = 0.0
    }
    
    // MARK: - Send Message Action
    
    @IBAction func sendMessage(_ sender: UIButton) {
        
        let validatedString = chatViewModel.textValidationFor(textViewChat)
        if validatedString != "" {
            chatViewModel.scrollToBottom(self.chatTableView, animated: true)
            let jobId = clModel.jobid ?? ""
            
            chatViewModel.sendMessageAPI(jobId, message: validatedString, type: .text)
            
            textViewChat.text = ""
        }
    }
    
    // MARK: - Send Image Action
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            let imagePicker = OpalImagePickerController()
            imagePicker.maximumSelectionsAllowed = 4
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func imageButtonTapped(_ sender: UIButton) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func selectDocumentAction(_ sender: UIButton) {
        
        let types: NSArray = NSArray(object: kUTTypePDF as NSString)
        let documentPicker = UIDocumentPickerViewController(documentTypes: types as! [String], in: .import)
        //documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    //MARK: - Pull To Refresh Selector
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        if chatViewModel.numberOfRows >= 20*offsetMultiplier {
            let jobId = clModel.jobid ?? ""
            chatViewModel.getPreviousChatFor(jobId, offset: "\(20*offsetMultiplier)")
            offsetMultiplier += offsetMultiplier
        }
        
        refreshControl.endRefreshing()
    }
    
}
// MARK: - UITableView Delegates & Datasource

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
}

extension ChatViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatViewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatObj = chatViewModel.getMessage(indexPath)
        
        switch chatObj.1 {
        case .Admin:
            switch chatObj.0.type {
            case .image:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell", for: indexPath) as? ChatImageCell {
                    //cell.chatCollectionView.delegate = self
                    cell.configureCell(chatObj.0.file, owner: chatObj.1)
                    return cell
                }
            case .text:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "OfficeChatViewCell", for: indexPath) as? OfficeChatViewCell {
                    cell.configureCell(chatObj.0)
                    return cell
                }
                
            }
        case .Technician:
            
            switch chatObj.0.type {
            case .image:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell", for: indexPath) as? ChatImageCell {
                    //cell.chatCollectionView.delegate = self
                    cell.configureCell(chatObj.0.file, owner: chatObj.1)
                    return cell
                }
            case .text:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "UserChatViewCell", for: indexPath) as? UserChatViewCell {
                    cell.configureCell(chatObj.0)
                    return cell
                }
            }
            
        }
        
        return UITableViewCell()
    }
    
}

// MARK: - UIImagePickerController Delegates

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let resizedImage = image.compressImage() {
            
            let jobId = clModel.jobid ?? ""
            chatViewModel.sendMessageAPI(jobId, message: resizedImage, type: .photo)
            
        } else {
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChatViewController: OpalImagePickerControllerDelegate {
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        
        let jobId = clModel.jobid ?? ""
        chatViewModel.sendMessageAPI(jobId, message: images, type: .photo)
        
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
