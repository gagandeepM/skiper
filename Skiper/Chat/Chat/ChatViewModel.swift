//
//  ChatViewModel.swift
//  Skiper
//
//  Created by Mayank juyal on 05/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class ChatViewModel {
    
    private lazy var sendViewModel: SendMessageModel = {
        return SendMessageModel()
    }()
    
    
    var chats: [Chat] = [Chat]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var alertMessage: String = String() {
        didSet{
            self.showAlertMessage?()
        }
    }
    
    var isLoading: Bool = false {
        didSet{
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfRows: Int {
        return chats.count
    }
    
    var updateLoadingStatus: (()->())?
    var showAlertMessage:(()->())?
    var reloadTableViewClosure:(()->())?
    
    
    private var maxHeight: CGFloat = 80.0
    
    //TODO: - Validate text
    func textValidationFor(_ textView: UITextView) -> String {
        let text = textView.text ?? ""
        let trimmedString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmedString.isEmpty) {
            return ""
        } else {
            return text.trimmingCharacters(in: .newlines)
        }
    }
    
    // Scroll table view to bottom
    
    func scrollToBottom(_ tableView: UITableView, animated: Bool) {
        DispatchQueue.main.async {
            
            let numberOfSections = tableView.numberOfSections
            let numberOfRows = tableView.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: !animated)
            }
        }
    }
    // Calculate Keyboard Height
    func calculateKeyboardHeight(_ keyboardFrame: NSValue) -> CGFloat {
        let keyboardRectangle = keyboardFrame.cgRectValue
        return keyboardRectangle.height
    }
    
    func calculateConstraintMaxHeight(_ textView: UITextView) -> CGFloat {
        if textView.text.contains(UIPasteboard.general.string ?? "") {
            if textView.contentSize.height < maxHeight {
                let height = textView.contentSize.height+10
                return height
            } else {
                return 80
            }
        } else {
            if textView.contentSize.height < maxHeight {
                let height = textView.contentSize.height+10
                return height
            } else {
                return 80
            }
        }
    }
    
    func getPreviousChatFor(_ jobId: String, offset: String = "0") {
        isLoading = true
        let url = SkiperURL.getSingleChat
        let parameter = self.generateParametersFor(jobId, offset: offset)
        HTTPClientAPI.instance().makeAPICall(url: url, params: parameter, method: .POST, success: { (data, response, error) in
            self.isLoading = false
            if error == nil {
                guard let data = data else {
                    self.alertMessage = MessageStrings.corruptData
                    return
                }
                self.gettingChat(data, updateType: .Append)
            } else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
            
        }) { (data, response, error) in
            
            self.isLoading = false
        }
    }
    private func generateParametersFor(_ jobId: String, offset: String) -> Dictionary<String, Any> {
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let parameter = ["token": token, "userId": technicianId, "job_id": jobId, "offset": offset]
        return parameter
    }
    
    private func gettingChat(_ data: Data, updateType: UpdateType) {
        let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        if let chatResponse = json as? [String: Any], let status = chatResponse["status"] as? Int, let message = chatResponse["message"] as? String {
            print(chatResponse)
            if status == 1 {
                if let chatData = chatResponse["data"] as? [[String: Any]] {
                    for item in chatData {
                        let chat = settingModelUsing(item)
                        update(chat, updateType: updateType)
                    }
                }
            } else if status == 2 {
                if #available(iOS 10.0, *) {
                    AppDelegate.appInstance.logOut()
                } else {
                    // Fallback on earlier versions
                }
            } else {
                self.alertMessage = message
            }
        }
    }
    
    private func update(_ chat: Chat, updateType: UpdateType) {
        switch updateType {
        case .Append:
            chats.append(chat)
        case .Insert:
            chats.insert(chat, at: 0)
       	        }
    }
    
    private func settingModelUsing(_ item: [String: Any]) -> Chat {
        
        let id = item["id"] as? String ?? ""
        
        let userId = item["user_id"] as? String ?? ""
        
        let participant = settingUpParticipant(userId)
        
        let jobId = item["job_id"] as? String ?? ""
        
        let messageContent = item["message"] as? [String: Any] ?? ["": ""]
        
        print("Message Content\(messageContent)")
        
        let url = item["url"] as? String ?? ""
        
        let message = self.settingMessageContent(messageContent, url: url)
        
        let notes = item["notes"] as? String ?? ""
        
        let seen = item["seen"] as? String ?? ""
        
        let date = item["date"] as? String ?? ""
        
        let firstName = item["FirstName"] as? String ?? ""
        
        let chat = Chat(id: id, participant: participant ,jobId: jobId, message: message, notes: notes, seen: seen, date: date, firstName: firstName, url: url)
        
        return chat
    }
    
    private func settingUpParticipant(_ userId: String) -> MessageOwner {
        if userId == "" {
            return .Technician
        } else {
            return .Admin
        }
    }
    
    private func settingMessageContent(_ message: [String: Any], url: String) -> Message {
      
        var files: [File] = [File]()
        let text = message["text"] as? String ?? ""
        let file = message["files"] as? [String] ?? [""]
        if url == "" {
            let messageModel = Message(text: text, file: files, type: .text)
            return messageModel
        } else {
            for item in file {
                let fullUrl = url+item
                let file = File(file: fullUrl)
                files.append(file)
            }
            let messageModel = Message(text: text, file: files, type: .image)
            return messageModel
        }
    }
    
    func getMessage(_ indexPath: IndexPath) -> (Message, MessageOwner) {
        let chat = chats.reversed()[indexPath.row]
        let message = chat.message
        let participant = chat.participant
        return(message, participant)
    }
    
    func sendMessageAPI(_ jobId: String, message: Any, type: MessageType) {
        self.isLoading = true
        
        sendViewModel.sendMessageAPI(jobId, messageContent: message, type: type) { [weak self] (data, error) in
            self?.isLoading = false
            if error == nil {
                guard let data = data else {
                    self?.alertMessage = MessageStrings.corruptData
                    return
                }
                self?.gettingChat(data, updateType: .Insert)
            } else {
                self?.alertMessage = error?.localizedDescription ?? ""
            }
        }
        
    }
    
    func recieveMessage(_ messageId: String) {
        isLoading = true
        let url = SkiperURL.getSingleMessage
        let parameter = self.parametersForRecievedMessage(messageId)
        HTTPClientAPI.instance().makeAPICall(url: url, params: parameter, method: .POST, success: { (data, response, error) in
            self.isLoading = false
            if error == nil {
                guard let data = data else {
                    self.alertMessage = MessageStrings.corruptData
                    return
                }
                self.gettingChat(data, updateType: .Insert)
            } else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
            
        }) { (data, response, error) in
            
            self.isLoading = false
        }
    }
    private func parametersForRecievedMessage(_ messageId: String) -> Dictionary<String, Any> {
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let parameter = ["token": token, "userId": technicianId, "message_id": messageId]
        return parameter
    }
    
}
