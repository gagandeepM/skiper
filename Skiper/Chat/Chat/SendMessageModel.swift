//
//  SendMessageModel.swift
//  Skiper
//
//  Created by Mayank juyal on 10/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

class SendMessageModel: ChatViewModel {
    
    func sendMessageAPI(_ jobId: String, messageContent: Any, type: MessageType, completion: @escaping (Data?, NSError?) -> Void) {
        let url = SkiperURL.sendMessage
        var parameters: Dictionary<String, Any> = ["": ""]
        var data: [Data] = [Data]()
        switch type {
        case .text:
            parameters = generateParameter(jobId, message: messageContent as? String ?? "", messageType: 0, data: nil)
            break
        case .photo:
            if let images = messageContent as? [UIImage] {
                
                for image in images {
                    data.append(generateData(content: image))
                }
                
                parameters = generateParameter(jobId, message: "Image from technician", messageType: 1, data: data)
            } else if let image = messageContent as? UIImage {
                data.append(generateData(content: image))
                parameters = generateParameter(jobId, message: "Image from technician", messageType: 1, data: data)
            }
            
            break
        case .file:
            parameters = generateParameter(jobId, message: messageContent as? String ?? "", messageType: 1, data: nil)
            break
        }
        
        //let parameter = generateParameter(jobId, message: message, data: data)
        
        HTTPClientAPI.instance().uploadFileToServer(url: url, parameters: parameters, data: data, method: .POST, format: type, success: { (data, response, error) in
            completion(data, error)
        }) { (data, response, error) in
            self.isLoading = false
        }
        
    }
    
    private func generateData(content: UIImage)-> Data {
        let data = UIImageJPEGRepresentation(content, 0.7)
        return data ?? Data()
    }
    
    
    private func generateParameter(_ jobId: String, message: String, messageType: Int, data: [Data]? = nil) -> Dictionary<String, Any> {
        
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        
        let parameter = ["userId": technicianId, "job_id": jobId, "message": message, "messageType": messageType, "token": token] as [String : Any]
        return parameter
    }
}
