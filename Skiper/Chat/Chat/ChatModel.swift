//
//  ChatModel.swift
//  Skiper
//
//  Created by Mayank juyal on 10/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import Foundation

enum ReceivedMessageType: String {
    case text
    case image
}

enum MessageType: String {
    case photo
    case text
    case file
}

enum UpdateType: String {
    case Append
    case Insert
}

enum MessageOwner: String {
    case Technician
    case Admin
}

struct File {
    var file: String?
}

struct Message {
    var text: String?
    var file: [File]
    var type: ReceivedMessageType
}

struct Chat {
    var id: String?
    var participant: MessageOwner
    var jobId: String?
    var message: Message
    var notes: String?
    var seen: String?
    var date: String?
    var firstName: String?
    var url: String?
}
