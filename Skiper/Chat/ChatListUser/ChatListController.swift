//
//  ChatListController.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChatListController: UITableViewController {
    
    fileprivate lazy var clViewModel: CLViewModel = {
        return CLViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        initViewModel()
    }
    
    private func initViewModel() {
        
        clViewModel.updateLoadingStatus = { [weak self] in
            let loading = self?.clViewModel.isLoading ?? false
            if loading {
                SVProgressHUD.show()
            } else {
                self?.hideProgressHud()
            }
        }
        
        clViewModel.showAlertMessage = { [weak self] in
            if let message = self?.clViewModel.alertMessage {
                self?.showAlert(message)
            }
        }
        
        clViewModel.reloadTableViewClosure = {[weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        clViewModel.callListAPI()
    }
    
    private func hideProgressHud() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    private func showAlert(_ message: String) {
        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clViewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as? CLCell {
            let clObject = self.clViewModel.gettingDetailsFor(indexPath)
            cell.titleCLLabel.text = clObject.jobTypeName
            cell.countLabel.text = clObject.lastData?.count ?? ""
            if clObject.lastData?.count == "0" || clObject.lastData?.count == "" {
                cell.countLabel.isHidden = true
            }else{
                cell.countLabel.isHidden = false
            }
            cell.subTitleCLLabel.text = clObject.lastData?.message ?? "No new message..."
            return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let chatController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            chatController.clModel = self.clViewModel.gettingDetailsFor(indexPath)
            self.navigationController?.pushViewController(chatController, animated: true)
        }
    }
}
