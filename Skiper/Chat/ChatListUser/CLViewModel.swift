//
//  CLViewModel.swift
//  Skiper
//
//  Created by Mayank juyal on 09/03/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

struct LastData {
    var count: String?
    var message: String?
}

struct CLModel {
    var jobDetails: String?
    var jobid: String?
    var jobTypeName: String?
    var lastData: LastData?
}

class CLViewModel: NSObject {
    
    private var clModelArray: [CLModel] = [CLModel]() {
        didSet{
            self.reloadTableViewClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String = "" {
        didSet{
            self.showAlertMessage?()
        }
    }
    
    var reloadTableViewClosure:(()->())?
    
    var showAlertMessage:(()->())?
    
    var updateLoadingStatus: (()->())?
    
    var numberOfRows: Int {
        return clModelArray.count
    }
    
    func callListAPI() {
        self.isLoading = true
        clModelArray.removeAll()
        let parameters = generateParamters()
        HTTPClientAPI.instance().makeAPICall(url: SkiperURL.getChatList, params: parameters, method: .POST, success: { (data, response, error) in
            
            self.isLoading = false
            if error == nil {
                guard let data = data else {
                    self.alertMessage = MessageStrings.corruptData
                    return
                }
                self.gettingObjectFrom(data)
            } else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        }) { (data, response, error) in
            self.isLoading = false
        }
    }
    
    private func gettingObjectFrom(_ data: Data) {
        let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        if let jsonObject = json as? [String: Any], let status = jsonObject["status"] as? Int, let message = jsonObject["message"] as? String {
            if status == 1 {
                if let responseData = jsonObject["data"] as? [[String: Any]] {
                    for item in responseData {
                        let jobDetails = item["Job_Details"] as? String ?? ""
                        let jobId = item["job_id"] as? String ?? ""
                        let jobTypeName = item["job_type_name"] as? String ?? ""
                        
                        if let lastData = item["last_data"] as? [String: Any], let message = lastData["message"] as? [String: Any], let count = lastData["countt"] as? String, let urlString = lastData["url"] as? String {
                            let text = settingMessageContent(message, url: urlString)
                            let lastObj = LastData(count: count, message: text)
                            clModelArray.append(CLModel(jobDetails: jobDetails, jobid: jobId, jobTypeName: jobTypeName, lastData: lastObj))
                        } else {
                            let lastObj = LastData(count: "", message: "No new message...")
                            clModelArray.append(CLModel(jobDetails: jobDetails, jobid: jobId, jobTypeName: jobTypeName, lastData: lastObj))
                        }
                    }
                }
            } else if status == 2 {
                if #available(iOS 10.0, *) {
                    AppDelegate.appInstance.logOut()
                } else {
                    // Fallback on earlier versions
                }
            } else {
                self.alertMessage = message
            }
        }
    }
    
    private func settingMessageContent(_ message: [String: Any], url: String) -> String {
        
        let text = message["text"] as? String ?? ""
        if url == "" {
            return text
        } else {
            return "file"
        }
    }
    
    private func generateParamters() -> Dictionary<String, Any> {
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let parameters = ["userId": technicianId, "token": token]
        return parameters
    }
    
    func gettingDetailsFor(_ indexPath: IndexPath) -> CLModel {
        let clObject = clModelArray[indexPath.row]
        return clObject
    }
}
