//
//  NotificationModel.swift
//  Skiper
//
//  Created by Mayank juyal on 24/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit

struct NotificationData {
    var job_id: String
    var appoinent_time: String
    var job_type_name: String
    var city: String
    var state: String
}

class NotificationFetch {
    
    typealias CompletionHandler = (_ success:Bool, _ message: String, _ notificationArray: [NotificationData]?) -> Void
    
    func fetchData(response: Any?, completion: CompletionHandler) {
        
        if let fetchedResponse = response as? [String: Any], let status = fetchedResponse["status"] as? Int, let message = fetchedResponse["message"] as? String {
            if status == 1 {
                var list = [NotificationData]()
                if let data = fetchedResponse["data"] as? [[String: Any]] {
                    for item in data {
                        let jobId = item["job_id"] as? String ?? ""
                        let appointmentTime = item["appoinent_time"] as? String ?? ""
                        let jobTypeName = item["job_type_name"] as? String ?? ""
                        let city = item["City"] as? String ?? ""
                        let state = item["State"] as? String ?? ""
                        let obj = NotificationData(job_id: jobId, appoinent_time: appointmentTime, job_type_name: jobTypeName, city: city, state: state)
                        list.append(obj)
                    }
                    completion(true, message, list)
                }
            } else if status == 2 {
                if #available(iOS 10.0, *) {
                    AppDelegate.appInstance.logOut()
                } else {
                    // Fallback on earlier versions
                }
            } else {
                completion(false, message, nil)
            }
        }
    }
}
