//
//  NotificationViewController.swift
//  Skiper
//
//  Created by Mayank juyal on 07/02/18.
//  Copyright © 2018 Gagandeep Mishra. All rights reserved.
//

import UIKit
import SVProgressHUD


class NotificationViewController: UITableViewController {
    
    private var badgeCount = ""
    
    fileprivate var notification: [NotificationData] = [NotificationData]() {
        didSet {
            self.reloadingTableView()
        }
    }
    
    lazy var fetchModel: NotificationFetch = {
        return NotificationFetch()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        settingGetNotificationParameters()
    }
    
    func settingGetNotificationParameters() {
        SVProgressHUD.show()
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let urlString = SkiperURL.getJobNotification
        let parameters = ["userId": technicianId, "token": token]
        callGetNotificationAPI(urlString: urlString, parameter: parameters)
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    
    private func reloadingTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if notification.count == 0 {
            AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "No job notification.")
        }
    }
    
    // MARK: - Get Notification List
    
    func callGetNotificationAPI(urlString: String, parameter: Dictionary<String, Any>) {
        HTTPClientAPI.instance().makeAPICall(url: urlString, params: parameter, method: .POST, success: { (data, response, error) in
            self.hideLoader()
            if error == nil {
                guard let data = data else {return}
                let response = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                self.fetchModel.fetchData(response: response, completion: {(success, message, list) -> Void in
                    if success {
                        if let list = list {
                            self.notification = list
                        } else {
                            if #available(iOS 10.0, *) {
                                AppDelegate.appInstance.logOut()
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    } else {
                        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
                    }
                })
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(String(describing: error?.localizedDescription))")
            }
        }) { (data, response, error) in
            self.hideLoader()
            if error == nil {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(response?.statusCode ?? 0)" )
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notification.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell" , for: indexPath) as? NotificationViewCell {
            let notificationData = notification[indexPath.row]
            cell.buttonCustomDelegate = self
            cell.configureCell(indexPath: indexPath, notificationData: notificationData)
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - Accept Reject API
    
    func callAcceptRejectAPI(parameter: Dictionary<String, Any>) {
        HTTPClientAPI.instance().makeAPICall(url: SkiperURL.acceptRejectJob, params: parameter, method: .POST, success: { (data, response, error) in
            self.hideLoader()
            if error == nil {
                guard let data = data else {return}
                let response = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let response = response as? [String: Any], let status = response["status"] as? Int, let message = response["message"] as? String {
                    if status == 1 {
                        self.settingGetNotificationParameters()
                    } else if status == 2 {
                        if #available(iOS 10.0, *) {
                            AppDelegate.appInstance.logOut()
                        } else {
                            // Fallback on earlier versions
                        }
                    } else {
                        AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: message)
                    }
                }
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }) { (data, response, error) in
            self.hideLoader()
            if error == nil {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: "\(response?.statusCode ?? 0)" )
            } else {
                AlertMessage.displayAlertMessage(titleMessage: "", alertMsg: error?.localizedDescription ?? "")
            }
        }
    }
    
}

// MARK: - Custom Button Delegate

extension NotificationViewController: ButtonActionDelegate {
    
    func tappedOnButton(status value: String, id: Int) {
        let technicianId = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.technicianId) as? String ?? ""
        let token = UserDefaultsConstant.userDefaults.value(forKey: UserDefaultsConstant.token) as? String ?? ""
        let jobId = notification[id].job_id
        let parameter = ["userId": technicianId, "job_id": jobId, "token": token, "accept_reject": value]
        SVProgressHUD.show()
        callAcceptRejectAPI(parameter: parameter)
    }
}
